-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 18, 2014 at 02:00 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `museum`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_attemps`
--

CREATE TABLE IF NOT EXISTS `failed_attemps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `email` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `failed_attemps`
--

INSERT INTO `failed_attemps` (`id`, `user_id`, `email`) VALUES
(14, 36, 'frankdevries@gmail.com'),
(15, 36, 'frankdevries@gmail.com'),
(16, 36, 'frankdevries@gmail.com'),
(17, 36, 'frankdevries@gmail.com'),
(18, 0, ''),
(19, 0, ''),
(20, 0, ''),
(21, 0, ''),
(22, 0, ''),
(23, 0, ''),
(24, 0, ''),
(25, 34, 'dae@gmail.com'),
(26, 34, 'dae@gmail.com'),
(27, 34, 'dae@gmail.com'),
(28, 34, 'dae@gmail.com'),
(29, 34, 'dae@gmail.com'),
(30, 34, 'dae@gmail.com'),
(31, 34, 'dae@gmail.com'),
(32, 34, 'dae@gmail.com'),
(33, 34, 'dae@gmail.com'),
(34, 34, 'dae@gmail.com'),
(35, 34, 'dae@gmail.com'),
(36, 36, 'frankdevries@gmail.com'),
(37, 36, 'frankdevries@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `filmtickets`
--

CREATE TABLE IF NOT EXISTS `filmtickets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titel` varchar(50) NOT NULL,
  `speeling` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `film_tickets`
--

CREATE TABLE IF NOT EXISTS `film_tickets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `naam` varchar(50) NOT NULL,
  `tussenvoegsel` varchar(50) NOT NULL,
  `achternaam` varchar(50) NOT NULL,
  `geboortedatum` date NOT NULL,
  `woonplaats` varchar(50) NOT NULL,
  `postcode` varchar(6) NOT NULL,
  `gekocht` timestamp DEFAULT CURRENT_TIMESTAMP,
  `prijs` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE IF NOT EXISTS `media` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `location` varchar(100) NOT NULL,
  `type` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `location`, `type`) VALUES
(14, 'artifect_1.jpg', 'image/jpeg'),
(17, 'artifect_3.jpg', 'image/jpeg'),
(18, 'artifect_4.jpg', 'image/jpeg'),
(19, 'artifect_5.jpg', 'image/jpeg'),
(20, 'artifect_6.jpg', 'image/jpeg'),
(21, 'artifect_7.jpg', 'image/jpeg'),
(22, 'Koala.jpg', 'image/jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `movies`
--

CREATE TABLE IF NOT EXISTS `movies` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `titel` varchar(50) NOT NULL,
  `speeling` varchar(20) NOT NULL,
  `beschikbaar` enum('yes','no') NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `movies`
--

INSERT INTO `movies` (`id`, `titel`, `speeling`, `beschikbaar`) VALUES
(1, 'A Frozen World', '08–12–2014 om 12:00', 'yes'),
(2, 'Turtle: The Incredible Journey ', '12–12–2014 om 12:00', 'yes'),
(3, 'Island of Lemurs:Madagascar 3D', '12–12–2014 om 14:00', 'yes'),
(4, 'Earth', '15–12–2014 om 12:00', 'yes'),
(5, 'A Frozen World', '15–12–2014 om 12:00', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `personeel`
--

CREATE TABLE IF NOT EXISTS `personeel` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `naam` varchar(20) NOT NULL,
  `achternaam` varchar(20) NOT NULL,
  `functie` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `personeel`
--

INSERT INTO `personeel` (`id`, `naam`, `achternaam`, `functie`) VALUES
(1, 'Frank', 'de Vries', 'verkoper'),
(2, 'Dahir', 'Warsame', 'manager'),
(3, 'daenerys', 'Targaryen', 'verkoper');

-- --------------------------------------------------------

--
-- Table structure for table `seats`
--

CREATE TABLE IF NOT EXISTS `seats` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `seats` int(11) NOT NULL,
  `movie_titel` varchar(100) NOT NULL,
  `gekocht` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `prijs` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `seats`
--

INSERT INTO `seats` (`id`, `seats`, `movie_titel`, `gekocht`, `prijs`) VALUES
(1, 1, 'Island of Lemurs:Madagascar 3D 12–12–2014 om 14:00', '2014-11-28 06:52:35', 7),
(2, 1, 'Island of Lemurs:Madagascar 3D 12–12–2014 om 14:00', '2014-11-28 06:52:35', 0),
(3, 5, 'Island of Lemurs:Madagascar 3D 12–12–2014 om 14:00', '2014-11-28 06:52:35', 0),
(4, 1, 'A Frozen World', '2014-11-28 06:52:35', 7),
(5, 3, 'A Frozen World', '2014-11-28 06:52:35', 7),
(6, 5, 'Earth', '2014-11-28 07:01:42', 35),
(7, 2, 'Island of Lemurs:Madagascar 3D', '2014-11-28 07:03:39', 0),
(8, 2, 'Island of Lemurs:Madagascar 3D', '2014-11-28 07:04:07', 0),
(9, 2, 'A Frozen World', '2014-11-28 07:09:46', 7),
(10, 1, 'A Frozen World', '2014-11-28 07:10:37', 7),
(11, 1, 'A Frozen World', '2014-11-28 07:10:59', 7),
(12, 1, 'A Frozen World', '2014-11-28 07:11:15', 7),
(13, 1, 'A Frozen World', '2014-11-28 07:12:43', 7),
(14, 1, 'A Frozen World', '2014-11-28 07:12:53', 7),
(15, 1, 'A Frozen World', '2014-11-28 07:13:07', 7),
(16, 5, 'Earth', '2014-11-28 07:14:28', 0),
(17, 5, 'Earth', '2014-11-28 07:16:56', 0),
(18, 5, 'Earth', '2014-11-28 07:17:04', 0),
(19, 1, 'A Frozen World', '2014-11-28 07:17:43', 0),
(20, 1, 'A Frozen World', '2014-11-28 07:19:38', 0),
(21, 1, 'A Frozen World', '2014-11-28 07:20:29', 0),
(22, 1, 'A Frozen World', '2014-11-28 07:20:55', 0),
(23, 1, 'A Frozen World', '2014-11-28 07:22:06', 7),
(24, 5, 'A Frozen World', '2014-11-28 07:23:00', 35),
(25, 1, 'A Frozen World', '2014-11-28 08:12:45', 7),
(26, 5, 'Turtle: The Incredible Journey ', '2014-11-28 08:23:39', 35),
(27, 5, 'A Frozen World', '2014-11-28 08:24:08', 35),
(28, 5, 'A Frozen World', '2014-11-28 08:24:15', 35),
(29, 5, 'Turtle: The Incredible Journey ', '2014-11-28 08:24:21', 35),
(30, 5, 'Turtle: The Incredible Journey ', '2014-11-28 08:24:29', 35),
(31, 5, 'Turtle: The Incredible Journey ', '2014-11-28 08:24:37', 35),
(32, 5, 'Turtle: The Incredible Journey ', '2014-11-28 08:24:43', 35),
(33, 20, 'Island of Lemurs:Madagascar 3D', '2014-11-28 08:25:04', 140),
(34, 20, 'Earth', '2014-11-28 08:25:22', 140),
(35, 1, 'A Frozen World', '2014-11-28 08:30:17', 0),
(36, 1, 'A Frozen World', '2014-11-28 08:31:43', 7),
(37, 1, 'A Frozen World', '2014-11-28 08:32:09', 0),
(38, 1, 'A Frozen World', '2014-11-28 08:34:00', 7),
(39, 5, 'A Frozen World', '2014-11-28 10:50:40', 35);

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

CREATE TABLE IF NOT EXISTS `tickets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `naam` varchar(50) NOT NULL,
  `tussenvoegsel` varchar(50) NOT NULL,
  `achternaam` varchar(50) NOT NULL,
  `geboortedatum` date NOT NULL,
  `woonplaats` varchar(50) NOT NULL,
  `postcode` varchar(6) NOT NULL,
  `gekocht` timestamp DEFAULT CURRENT_TIMESTAMP,
  `prijs` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=143 ;

--
-- Dumping data for table `tickets`
--

INSERT INTO `tickets` (`id`, `naam`, `tussenvoegsel`, `achternaam`, `geboortedatum`, `woonplaats`, `postcode`, `gekocht`, `prijs`) VALUES
(1, 'Dahir', '', 'warsame', '1995-04-24', 'Utrecht', '', '2014-11-15 13:16:00', 4),
(2, 'frank', 'de', 'vries', '1995-04-24', 'Utrecht', '', '2014-11-15 13:16:00', 0),
(3, 'bennie', '', 'Core', '1995-05-24', 'Sydney', '', '2014-11-16 13:24:25', 0),
(4, 'dahir', 'da', 'da', '1995-07-09', 'Heerlen', '', '2014-11-16 15:48:23', 0),
(5, 'dahir', 'da', 'da', '1995-07-09', 'Heerlen', '', '2014-11-16 15:49:08', 0),
(6, 'warsame', 'dahir', 'de vries', '1990-09-13', 'Zaandam', '', '2014-11-16 15:49:45', 35),
(7, 'warsame', 'dahir', 'de vries', '1990-09-13', 'Zaandam', '', '2014-11-16 15:50:25', 14),
(41, 'Dahir', 'Wrsa', 'WArsame', '1995-04-24', 'London', '', '2014-11-16 23:18:15', 0),
(42, 'Dahir', 'Wrsa', 'WArsame', '1995-04-24', 'London', '', '2014-11-16 23:18:42', 0),
(43, 'Dahir', 'Wrsa', 'WArsame', '1995-04-24', 'London', '', '2014-11-16 23:18:59', 0),
(44, 'Dahir', 'Wrsa', 'WArsame', '1995-04-24', 'London', '', '2014-11-16 23:20:00', 0),
(45, 'Dahir', 'Wrsa', 'WArsame', '1995-04-24', 'London', '', '2014-11-16 23:20:48', 0),
(46, 'Dahir', 'Wrsa', 'WArsame', '1995-04-24', 'London', '', '2014-11-16 23:21:53', 0),
(47, 'Dahir', 'Wrsa', 'WArsame', '1995-04-24', 'London', '', '2014-11-16 23:22:19', 0),
(48, 'Dahir', 'Wrsa', 'WArsame', '1995-04-24', 'London', '', '2014-11-16 23:23:30', 0),
(49, 'Dahir', 'Wrsa', 'WArsame', '1995-04-24', 'London', '', '2014-11-16 23:26:43', 0),
(84, 'Dahir', 'Wrsa', 'WArsame', '1995-04-24', 'London', '', '2014-11-17 01:36:18', 0),
(85, 'Dahir', 'Wrsa', 'WArsame', '1995-04-24', 'London', '', '2014-11-17 01:37:55', 0),
(86, 'Dahir', 'Wrsa', 'WArsame', '1995-04-24', 'London', '', '2014-11-17 01:38:44', 0),
(87, 'Dahir', 'Wrsa', 'WArsame', '1995-04-24', 'London', '', '2014-11-17 01:41:24', 0),
(88, 'Dahir', 'Wrsa', 'WArsame', '1995-04-24', 'London', '', '2014-11-17 01:42:19', 0),
(89, 'frank', 'van', 'vliet', '1999-04-04', 'London', '1324ad', '2014-11-17 01:50:23', 0),
(90, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 01:52:08', 0),
(91, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 02:24:40', 0),
(92, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 02:25:34', 0),
(93, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 02:27:29', 0),
(94, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 02:27:48', 0),
(95, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 02:28:03', 0),
(96, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 02:30:28', 0),
(97, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 02:30:38', 0),
(98, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 02:31:54', 0),
(99, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 02:32:06', 0),
(100, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 02:32:24', 0),
(101, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 02:38:33', 0),
(102, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 02:38:42', 0),
(103, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 02:38:54', 0),
(104, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 02:39:06', 0),
(105, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 02:41:45', 0),
(106, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 02:41:56', 0),
(107, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 02:42:17', 0),
(108, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 02:42:33', 0),
(109, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 02:42:47', 0),
(110, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 02:42:56', 0),
(111, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 02:43:39', 0),
(112, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 02:45:29', 0),
(113, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 02:45:42', 0),
(114, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 02:46:29', 0),
(115, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 02:47:20', 0),
(116, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 02:48:12', 0),
(117, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 02:48:25', 0),
(118, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 02:48:47', 0),
(119, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 02:50:38', 0),
(120, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 02:58:49', 0),
(121, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 02:59:01', 0),
(122, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 03:02:30', 0),
(123, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 03:02:37', 0),
(124, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 03:03:42', 0),
(125, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 03:03:44', 0),
(126, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 03:04:10', 0),
(127, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 03:04:22', 0),
(128, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 03:04:32', 0),
(129, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 03:04:40', 0),
(130, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 03:05:16', 0),
(131, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 03:07:40', 0),
(132, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 03:08:22', 4),
(133, 'Niek', '', 'Visser', '1993-04-21', 'Friesland', '1324ar', '2014-11-17 03:10:13', 4),
(134, 'Frank', 'den', 'Thomess', '1989-10-05', 'Rotterdam', '4578ad', '2014-11-17 03:19:24', 4),
(135, 'Frank', 'den', 'Thomess', '1989-10-05', 'Rotterdam', '4578ad', '2014-11-17 03:20:12', 4),
(136, 'Dahir', 'WArsame', 'Wasame', '1995-04-24', 'Terschelling', '1324as', '2014-11-17 04:47:06', 4),
(137, 'da', 'da', 'da', '0006-05-04', 'London', '1324ad', '2014-11-17 04:54:34', 3),
(138, 'ja', 'van de', 'Bergen', '1983-06-05', 'Utrecht', '1334AD', '2014-11-17 09:21:07', 4),
(139, 'Dahir', 'van', 'Warsame', '1995-04-24', 'Utrecht', '3562ad', '2014-11-26 23:29:08', 4),
(140, 'Fatima', 'den', 'Bergen', '1994-10-31', 'London', '1334AD', '2014-11-28 00:59:00', 4),
(141, 'dahir', '', 'warsame', '1995-04-04', 'Utrecht', '3562ad', '2014-11-28 10:44:17', 4),
(142, 'Hans', '', 'Odijk', '1995-03-15', 'London', '1324ad', '2014-11-28 10:47:51', 4);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `role` enum('user','admin','manager','verkoper') NOT NULL DEFAULT 'verkoper',
  `email` varchar(254) NOT NULL,
  `username` varchar(32) NOT NULL DEFAULT '',
  `status` enum('blocked','active') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_username` (`username`),
  UNIQUE KEY `uniq_email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=41 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role`, `email`, `username`, `status`) VALUES
(1, 'manager', 'dahirwarsame@gmail.com', 'dahirwarsame@gmail.com', 'active'),
(34, 'verkoper', 'dae@gmail.com', 'dae@gmail.com', 'blocked'),
(36, 'verkoper', 'frankdevries@gmail.com', 'frankdevries@gmail.com', 'active'),
(39, 'manager', 'admin@admin.com', 'admin@admin.com', 'active'),
(40, 'verkoper', 'verkoper@sale.com', 'verkoper', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `verkopers`
--

CREATE TABLE IF NOT EXISTS `verkopers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `naam` varchar(20) NOT NULL,
  `achternaam` varchar(20) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `verkopers`
--

INSERT INTO `verkopers` (`id`, `naam`, `achternaam`, `user_id`) VALUES
(1, 'Frank', 'de Vries', 40);

-- --------------------------------------------------------

--
-- Table structure for table `wachtwoorden`
--

CREATE TABLE IF NOT EXISTS `wachtwoorden` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `wachtwoord` varchar(32) NOT NULL,
  PRIMARY KEY (`id`,`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `wachtwoorden`
--

INSERT INTO `wachtwoorden` (`id`, `user_id`, `wachtwoord`) VALUES
(1, 34, 'dae'),
(2, 40, 'sale'),
(3, 1, 'd'),
(4, 36, 'frankiefrost'),
(5, 39, 'admin'),
(6, 44, 'awesome'),
(7, 44, 'awesome'),
(8, 44, 'awesome'),
(9, 44, 'awesome'),
(10, 44, 'awesome'),
(11, 44, 'awesome'),
(12, 44, 'awesome'),
(13, 44, 'awesome'),
(14, 44, 'awesome'),
(15, 44, 'awesome'),
(16, 44, 'awesome'),
(17, 44, 'awesome'),
(18, 44, 'awesome'),
(19, 44, 'awesome'),
(20, 44, 'awesome'),
(21, 44, 'awesome'),
(22, 5, 'awesome'),
(23, 5, 'awesome');
SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
