<?php
/**
 * Created by PhpStorm.
 * User: Eigenaar
 * Date: 16-11-2014
 * Time: 0:53
 */

class Overzicht_model  extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function tickets_today()
    {
        $query = $this->db->query("SELECT * FROM tickets WHERE DATE(`gekocht`) = CURDATE() ORDER BY `gekocht` DESC");
        return $query->result();
    }
    public function getAmount()
    {
        $query =  $this->db->count_all('tickets');
        return ($query);

    }
    public function tickets()
    {
        $query = $this->db->query("SELECT * FROM tickets");
        return $query->result();
    }
    public function tickets_lastweek()
    {
        $query = $this->db->query("SELECT * FROM tickets WHERE WEEKOFYEAR(`gekocht`)=WEEKOFYEAR(NOW())-1 ORDER BY `gekocht` DESC");#
        return $query->result();
    }
    public function tickets_last_3months()
    {
        $query = $this->db->query("SELECT * FROM tickets WHERE `gekocht` > DATE_SUB(NOW(), INTERVAL 3 MONTH) ORDER BY `woonplaats` ASC"); #
        return $query->result();
    }

}