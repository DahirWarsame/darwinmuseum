<?php
/**
 * Created by PhpStorm.
 * User: Eigenaar
 * Date: 16-11-2014
 * Time: 0:53
 */

class Ticket_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    public function getAll()
    {
        $query = $this->db->get('tickets');
        return $query->result();
    }

    public function getAllMovies()
    {
        $query = $this->db->get('movies');
        return $query->result();
    }

    public function get_ticket_data()
    {
        $query = $this->db->query("SELECT * FROM tickets ORDER BY gekocht DESC LIMIT 1");
        return $query->result();
    }

    public function alter_ticket($prijs, $id)
    {
        $this->db
            ->set('prijs',$prijs)
            ->where('id', $id)
            ->update('tickets');
        return;
    }
    public function add_ticket($data)
    {
        $this->db->insert('tickets', $data);
        $this->db->insert_id();
        return;
    }

    public function delete()
    {
        $this->db->where('id', $this->uri->segment(3));
        $this->db->delete('data');
    }

    public function age($id)
    {
        $query = $this->db->query("SELECT 'geboortedatum' FROM tickets WHERE id = '".$id."'");
        $date = $query->result();
        return $date;
    }
    public function tickets_today()
    {
        $query = $this->db->query("SELECT * FROM tickets WHERE DATE('gekocht') = CURDATE() ORDER BY 'gekocht' DESC");
        return $query->result();
    }
    public function tickets_lastweek()
    {
        $query = $this->db->query("SELECT * FROM tickets WHERE WEEKOFYEAR('gekocht')=WEEKOFYEAR(NOW())-1 ORDER BY 'gekocht' DESC");#
        return $query->result();
    }
    public function tickets_last_3months()
    {
        $query = $this->db->query("SELECT * FROM tickets WHERE 'gekocht' > DATE_SUB(NOW(), INTERVAL 3 MONTH) ORDER BY 'woonplaats' ASC");#
        return $query->result();
    }

}