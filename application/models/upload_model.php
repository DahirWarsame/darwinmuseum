<?php
/**
 * Created by PhpStorm.
 * User: Eigenaar
 * Date: 17-11-2014
 * Time: 5:40
 */

class Upload_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    function get_all()
    {
        $query = $this->db->get('media');
        return $query->result();
    }
    function insert_images($image_data  = array())
    {
        $data = array(
            'location' => $image_data['file_name'],
            'type' => $image_data['file_type']
        );

        $this->db->insert('media', $data);
    }
    function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('media');
    }
} 