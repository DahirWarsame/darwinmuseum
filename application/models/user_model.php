<?php
/**
 * Created by PhpStorm.
 * User: Eigenaar
 * Date: 23-11-2014
 * Time: 17:21
 */

class User_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }
    /**
     * @return array get all users from users table.
     */
    public function getall()
    {
        $query = $this->db->get('users');
        return $query->result();
    }
    public function getperson()
    {
        $query = $this->db->get('personeel');
        return $query->result();
    }
    public function get_blocked_user($mail)
    {
        $query = $this->db->query("SELECT `email`, `id`, `status` FROM `users` WHERE `email` = '$mail'");
        return $query->result();
    }
    /**
     * Attempts to validate and log a user in
     *
     * @param string $role manager or verkoper role
     * @param string $email
     * @param string $password do not encrypt
     *
     * @return array
     */
    public function login($role, $email, $password)
    {
        /*$query2 =  $this->db->get_where('users', array(
            'role' => $role,
            'email' => $email,
            'password' => $password
        ));*/
        $query = $this->db->query("SELECT us.email, ww.wachtwoord FROM users us
                                  LEFT JOIN wachtwoorden ww ON us.id=ww.user_id WHERE
                                  us.role = '$role' and us.email = '$email' and
                                  ww.wachtwoord = '$password'and ww.wachtwoord<>''");
        /*echo $this->db->_error_message();
        error_log("data in model AFTER INSERT: ");
        var_dump($query->result());*/
        return $query->result();
    }

    public function status($email){
        $query = $this->db->query("SELECT `status`, `id` FROM `users` WHERE `email` = '$email'");
        return $query->result();
    }

    public function failed_attemps($id,$email)
    {
        $query = $this->db->query("INSERT INTO `failed_attemps`(`user_id`, `email`) VALUES ('$id' , '$email')");
        $this->db->query($query);
       



    }
    public function update($id)
    {
        $query2 = $this->db->query("UPDATE users u INNER JOIN failed_attemps fa ON u.id = fa.user_id SET u.status = 'blocked' WHERE fa.user_id = $id");
        $this->db->query($query2);
    }
    public function count(){
        $query3 = $this->db->query("SELECT COUNT(*) FROM failed_attemps WHERE email = '$email'");
        return $query3->result();
    }
    /**
     * @param $email
     * @param $password
     * @return mixed
     */
    public function create($data)
    {
        $this->db->where('email', $data['email']);
        $duplicate = $this->db->count_all_results('users');

        if($duplicate >0)
        {
            return false;
        }

        //Create the record
        error_log("data in model BEFORE INSERT:" . json_encode($data));
        $insert =  $this->db->insert('users', $data);
        echo $this->db->_error_message();
        error_log("data in model AFTER INSERT: ");
        return $insert;

    }

    public function delete($id)
    {
        $this->db->where(['id' => $id], $this->uri->segment(3));
        $this->db->delete('user');
    }
    public function switch_activation($id)
    {
        /*UPDATE `museum`.`users` SET `status` = 'active' WHERE `users`.`id` = 1;*/
        /*$this->db->where(['id' => $id], $this->uri->segment(3));
        $this->db->delete('user');*/
        $this->db
            ->set('status','active')
            ->where(['id' => $id], $this->uri->segment(3))
            ->update('users');
        return;
    }
    public function get_userdata($email)
    {
        $query = $this->db->query("SELECT us.id, vk.naam, vk.achternaam FROM verkopers vk LEFT JOIN users us ON us.email='$email'");
        return $query->result();
    }

}
