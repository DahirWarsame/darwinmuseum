<?php
/**
 * Created by PhpStorm.
 * User: Eigenaar
 * Date: 24-11-2014
 * Time: 5:41
 */

class Film_model extends CI_Model{

    public function __construct()
    {
        $this->load->database();
    }

    /**
     * Haalt alle film van de Database
     *
     * @return array
     */
    public function getAllMovies()
    {
        $query = $this->db->get('movies');
        return $query->result();
    }
    public function getSeats()
    {
        $query =  $this->db->get('seats');
        return $query->result();

    }
    public function getAmountSeats($titel)
    {
        $query =  $this->db->query("SELECT count(seats) as seats FROM seats WHERE `movie_titel` = '$titel'");
        return $query->result();
    }

    /**
     * Insert record met de gegevens van db
     *
     * @param $data
     */
    public function add_ticket($data)
    {
        $this->db->insert('seats', $data);
        $this->db->insert_id();
        return;
    }
    public function get_ticket_data()
    {
        $query = $this->db->query("SELECT `seats`.id, `movies`.titel, `seats`.seats,`seats`.prijs FROM `seats`,`movies` where seats.movie_titel = movies.titel ORDER BY `seats`.gekocht DESC LIMIT 1");
        return $query->result();
    }

    public function alter_ticket($prijs, $titel ,$id)
    {
        $query = $this->db->query("UPDATE `museum`.`seats` SET `prijs` = '$prijs' WHERE `seats`.`movie_titel` = '$titel' and `seats`.`id` = $id");
        #echo "UPDATE `museum`.`seats` SET `prijs` = '$prijs' WHERE `seats`.`movie_titel` = '$titel' and `seats`.`id` = $id";
        $this->db->query($query);
    }
} 