<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Upload extends CI_Controller{


    function __construct()
    {
        parent::__construct();
        // Get the last segment in the URI, and only redirect out of the
        // protected area if it is NOT the login form
        $section = $this->uri->segment_array();
        array_shift($section);

        $section = end($this->uri->segment_array());
        if ($section != 'login' && $section != 'submit'
            && $this->session->userdata('is_admin') == false
        ) {
            redirect(site_url('manager/login'));
        }
    }
    function index()
    {
        $this->load->manager_template('upload/upload');
    }
    public function get_all()
    {
        $data['files'] = $this->upload_model->get_all();
        $this->load->manager_template('upload/gallary', $data);
    }
    function do_upload()
    {
        $config['upload_path'] = '../DarwinMuseum/uploads/img';
        $config['allowed_types'] = '*';
        $config['max_size'] = '100000';
        $config['max_width']  = '';
        $config['max_height']  = '';

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload())
        {
            $error = array('error' => $this->upload->display_errors());
            $this->upload_model->insert_images($this->upload->data());
            $this->load->manager_template('upload/upload', $error);
        }
        else
        {
            $this->upload_model->insert_images($this->upload->data());
            $data = array('upload_data' => $this->upload->data());
            $this->load->manager_template('upload/upload_succes', $data);
        }
    }
    
    function delete($id)
    {
        $this->upload_model->delete($id);
        $this->get_all();
    }
}
