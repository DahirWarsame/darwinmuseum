<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tickets extends CI_Controller {


    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        $this->load->template('tickets/bestel_tickets');
    }

    public function getAll()
    {
        $data['tickets'] = $this->ticket_model->getAll();
        $this->load->manager_template('tickets/all_tickets', $data);
    }
    public function add()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('naam', 'Naam', 'trim|required');
        $this->form_validation->set_rules('tussenvoegsel', 'Tussenvoegsel', 'trim');
        $this->form_validation->set_rules('achternaam', 'Achternaam', 'trim|required');
        $this->form_validation->set_rules('geboortedatum', 'Geboortedatum', 'required');
        $this->form_validation->set_rules('woonplaats', 'Woonplaats', 'trim|required');
        $this->form_validation->set_rules('postcode', 'Postcode', 'trim');

        if($this->form_validation->run() == FALSE)
        {
            $this->load->template('tickets/bestel_tickets');
        }
        else {
            $data = array(
                'naam' => $this->input->post('naam'),
                'tussenvoegsel' => $this->input->post('tussenvoegsel'),
                'achternaam' => $this->input->post('achternaam'),
                'geboortedatum' => $this->input->post('geboortedatum'),
                'woonplaats' => $this->input->post('woonplaats'),
                'postcode' => $this->input->post('postcode')

            );

            $this->ticket_model->add_ticket($data);
            $this->pay();
        }
    }

    public function addasVerkoper()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('naam', 'Naam', 'trim|required');
        $this->form_validation->set_rules('tussenvoegsel', 'Tussenvoegsel', 'trim');
        $this->form_validation->set_rules('achternaam', 'Achternaam', 'trim|required');
        $this->form_validation->set_rules('geboortedatum', 'Geboortedatum', 'required');
        $this->form_validation->set_rules('woonplaats', 'Woonplaats', 'trim');
        $this->form_validation->set_rules('postcode', 'Postcode', 'trim');

        if($this->form_validation->run() == FALSE)
        {
            $this->load->verkoper_template('tickets/bestel_tickets_verkoper');
        }

        else {
            $data = array(
                'naam' => $this->input->post('naam'),
                'tussenvoegsel' => $this->input->post('tussenvoegsel'),
                'achternaam' => $this->input->post('achternaam'),
                'geboortedatum' => $this->input->post('geboortedatum'),
                'woonplaats' => 'London',
                'postcode' => '1324ad'

            );

            $this->ticket_model->add_ticket($data);
            $this->pay();
        }
    }

    public function age()
    {
        $from = new DateTime($_POST['geboortedatum']);
        $to   = new DateTime('today');
        $age = $from->diff($to)->y;
        return $age;
    }

    public function pay()
    {
        $age = $this->age();
        $prijs = "";
        if($age <= 11)
        {
            $prijs = "0 euro";
        }
        elseif($age >= 18 && $age <= 59 )
        {
            $prijs = "4 euro";
        }
        elseif(($age >= 59) || ($age >= 12 && $age <= 17))
        {
            $prijs = "2.50 euro";
        }
        $price= array(
            'prijs' => $prijs
        );
        $data = $this->ticket_model->get_ticket_data();
        $this->ticket_model->alter_ticket($prijs,$data[0]->id);
        $this->load->template('tickets/betaal_tickets', $price);
    }

    public function print_ticket()
    {
        $data['query'] = $this->ticket_model->get_ticket_data();

        $this->load->template('tickets/print_ticket', $data);
    }
    public function get_pdf()
    {
        $this->load->library('mpdf');
        //get data from the model
        $data['query'] = $this->ticket_model->get_ticket_data();
        $mpdf = new mPDF('c', 'A4', '', '', 32, 25, 27, 25, 16, 13);
        $mpdf->SetDisplayMode('fullpage');

        //whether to indent the first level of a list
        $mpdf->list_indent_first_level = 0; // 1 or 0 -
        //load view for the pdf
        $html = $this->load->view('tickets/print_ticket_pdf', $data, TRUE);
        $mpdf->WriteHTML($html);
        $mpdf->Output('ticket.pdf', 'D');
    }
}

/* End of file tickets.php */
/* Location: ./application/controllers/tickets.php */