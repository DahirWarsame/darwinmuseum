<?php
/**
 * Created by PhpStorm.
 * User: Eigenaar
 * Date: 21-11-2014
 * Time: 9:59
 */


class User extends CI_controller{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->template('user/create_view');
    }

    public function create_user()
    {
        $data = array(
        'email' => $this->input->post('email'),
        'username' => $this->input->post('email'),
        'password' => $this->input->post('password')
        );
        //$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[TABLENAME.COLUMNNAME]');
        echo $this->user_model->create($data);
    }

    public function delete_user($id)
    {
        $this->load->model('user_model');
        $this->user_model->delete($id);
    }

} 