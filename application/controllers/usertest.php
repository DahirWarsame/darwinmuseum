<?php
require_once(APPPATH . '/controllers/test/Toast.php');
require_once(APPPATH . '/controllers/user.php'); //Require the tested class's file

class usertest extends Toast
{
    private $user;

    function __construct()
    {
        parent::__construct(__FILE__);
        $this->user = new User(true);    //Instantiate class
    }

    function _pre()
    {

        //Prepare unit test user database entry

        $data = array(
            'id' => '5',
            'username' => 'De Tester',
            'email' => 'test@gmail.com'
        );
        $this->db->insert('users', $data);

        $ticket = array(
            'user_id' => '5',
            'wachtwoord' => 'awesome'
        );
        $this->db->insert('wachtwoorden', $ticket);
    }


    function test_ticket()
    {
        $this->_assert_true($this->user->user_model->get_blocked_user('test@gmail.com'));
    }

    function _post()
    {

        //Remove unit test office from database
       $this->db->delete('users', array('id' => '5'));
    }
}