<?php
/**
 * Created by PhpStorm.
 * User: Eigenaar
 * Date: 21-11-2014
 * Time: 9:59
 */

class Manager extends CI_controller{
    public function __construct()
    {
        parent::__construct();
        // Get the last segment in the URI, and only redirect out of the
        // protected area if it is NOT the login form
        $section = $this->uri->segment_array();
        array_shift($section);

        $section = end($this->uri->segment_array());
        if ($section != 'login' && $section != 'submit'
                && $this->session->userdata('is_admin') == false
                ) {
            redirect(site_url('manager/login'));
        }
    }

    public function index()
    {
        redirect(site_url('manager/login'));
    }

    public function get(){
        $data['users'] = $this->user_model->getAll();
        $this->load->manager_template('manager/all_users', $data);
    }
    private function log( $status,$user,$role){
        $this->load->helper('file');
      /*$status = "GELUKT";
        $user = "Dahir@DAHIR.com";
        $role= "admin";*/
        date_default_timezone_set('Europe/Amsterdam');
        $date = date('m/d/Y h:i:s a', time());
        $data = $status.':: User: '."$user".' of userrole: '."$role".' has logged in on '.$date."\n";

        write_file('././assests/log/log.txt', $data, 'a+');
    }

    public function print_log()
    {
        $filename = '././assests/log/log.txt';
        $contents = file($filename);

        $data['contents'] = $contents;
        $this->load->manager_template('manager/log', $data);
    }

    public function login($submit = null)
    {
        if($submit == null)
        {
            $this->load->template('manager/login');
            return true;
        }
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $result =  $this->user_model->login('manager',$email,$password);

        if ($result == true)
        {
            $status = "SUCCES";
            $this->log($status,$email,"manager");
            $this->session->set_userdata('is_admin', 1);
            redirect(site_url('manager/dashboard'));
        } else
        {
            $status = "FAILED";
            $this->log($status,$email,"manager");
            redirect(site_url('manager/login'));
        }

        date_default_timezone_set('Europe/Amsterdam');
        $date = date('m/d/Y h:i:s a', time());
    }
    public function logout(){
        $this->session->sess_destroy();
        redirect(site_url('welcome/'));
    }
    public function dashboard()
    {
        redirect(site_url('overzicht'));
        

    }
    function activate($id)
    {
        $this->user_model->switch_activation($id);

    }
#000301364873
} 
