<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {


	public function index()
	{
        $data['movies'] = $this->film_model->getAllMovies();
        $this->load->template('welcome/welcome_message', $data);
	}
    public function about()
    {
        $this->load->template('welcome/about');
    }
    public function map()
    {
        $this->load->template('welcome/plattegrond');
    }
    public function persooneel()
    {
        $data['users'] = $this->user_model->getperson();
        $this->load->template('welcome/persooneel', $data);
    }


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */