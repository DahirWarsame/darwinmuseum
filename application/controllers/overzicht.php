<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Overzicht extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        // Get the last segment in the URI, and only redirect out of the
        // protected area if it is NOT the login form
        $section = $this->uri->segment_array();
        array_shift($section);

        $section = end($this->uri->segment_array());
        if ($section != 'login' && $section != 'submit'
            && $this->session->userdata('user_id') == false
            && $this->session->userdata('is_admin') == false
                ) {
            redirect(site_url('manager/login'));
        }

    }
    public function index()
    {
        $this->load->manager_template('overzicht/overzicht_tickets');
    }
    public function tickets_today()
    {
        #SELECT * FROM tickets WHERE DATE(`gekocht`) = CURDATE() ORDER BY 'gekocht' DESC
        $data['tickets'] = $this->overzicht_model->tickets_today();
        $this->load->manager_template('overzicht/tickets_overzicht_today', $data);

    }
    public function reserveringen()
    {
        #SELECT * FROM tickets WHERE DATE(`gekocht`) = CURDATE() ORDER BY 'gekocht' DESC
        $data['tickets'] = $this->overzicht_model->tickets();
        $data['tot_tickets'] = $this->overzicht_model->getAmount();
        $this->load->manager_template('overzicht/tickets_overzicht_reserveringen', $data);

    }
    public function tickets_lastweek()
    {

        #SELECT * FROM tickets WHERE WEEKOFYEAR(`gekocht`)=WEEKOFYEAR(NOW())-1 ORDER BY `gekocht` DESC
        $data['tickets'] = $this->overzicht_model->tickets_lastweek();
        $this->load->manager_template('overzicht/tickets_overzicht_lastweek', $data);
    }
    public function tickets_last_3months()
    {

        #SELECT * FROM tickets WHERE `gekocht` > DATE_SUB(NOW(), INTERVAL 3 MONTH) ORDER BY `woonplaats` ASC
        $data['tickets'] = $this->overzicht_model->tickets_last_3months();
        $this->load->manager_template('overzicht/tickets_overzicht_last_3months', $data);
    }
}

/* End of file tickets.php */
/* Location: ./application/controllers/tickets.php */