<?php
/**
 * Created by PhpStorm.
 * User: Eigenaar
 * Date: 27-11-2014
 * Time: 15:49
 */

class Verkoper extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        // Get the last segment in the URI, and only redirect out of the
        // protected area if it is NOT the login form
        $section = $this->uri->segment_array();
        array_shift($section);

        $section = end($this->uri->segment_array());
        if ($section != 'login' && $section != 'submit'
            && $this->session->userdata('user_id') == false
        ) {
            redirect(site_url('verkoper/login'));
        }
    }

    public function index()
    {
        redirect(site_url('verkoper/login'));
    }
    private function log( $status,$user,$role, $voornaam, $achternaam){
        $this->load->helper('file');
        /*$status = "GELUKT";
        $user = "Dahir@DAHIR.com";
        $role= "admin";*/
        date_default_timezone_set('Europe/Amsterdam');
        $date = date('m/d/Y h:i:s a', time());
        $data = $status.':: User: '."$voornaam"." "."$achternaam"." met username: $user".' of userrole: '."$role".' has logged in on '.$date."\n";

        write_file('././assests/log/log.txt', $data, 'a+');
    }

    /**
     *  print_log
     */
    public function print_log()
    {
        $filename = '././assests/log/log.txt';
        $contents = file($filename);

        $data['contents'] = $contents;
        $this->load->verkoper_template('manager/log', $data);
    }

    /**
     * @param null $submit gets login data and
     */
    public function login($submit = null)
    {
        if ($submit == null) {
            $this->load->template('verkoper/login');
            return true;
        }
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $userdata = $this->user_model->get_userdata($email);
        $status = $this->user_model->status($email);
        if ($status[0]->status == 'blocked') {
            echo ' <p class="bg-info">Je account is geblokkeerd</p>';
            $this->load->template('verkoper/login');
        } else {

            $result = $this->user_model->login('verkoper', $email, $password);

            if ($result == true) {
                $this->log("SUCCES", $email, "verkoper", $userdata[0]->naam,$userdata[0]->achternaam);
                $this->session->set_userdata('user_id', 1);
                redirect(site_url('verkoper/dashboard'));
            } else {
                $this->user_model->failed_attemps($status[0]->id,$email);
                $this->log("SUCCES", $email, "verkoper", $userdata[0]->naam,$userdata[0]->achternaam);
                redirect(site_url('verkoper/login'));
            }
        }
    }
    public function logout(){
        $this->session->sess_destroy();
        redirect(site_url('welcome/'));
    }

    public function dashboard()
    {
        $this->load->verkoper_template('verkoper/dashboard');
    }

} 