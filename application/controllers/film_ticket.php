<?php
/**
 * Created by PhpStorm.
 * User: Eigenaar
 * Date: 20-11-2014
 * Time: 13:18
 */

class Film_ticket extends CI_Controller{

    function __construct()
    {
        parent::__construct();

    }

   
    /**
     * haalt alle film op
     */
    function makeTicket()
    {
        $data['movies'] = $this->film_model->getAllMovies();
        $this->load->template('tickets/film_ticket', $data);
    }
    public function available(){
        $seats = $this->film_model->getAmountSeats();
        if( $seats[0]->seats >= 100){
            echo "Geen stoelen meer beschikbaar.";
        }
    }
    /**
     * Voegt film ticket toe
     */
    public function addFilmTicket()
    {
        /*$seats = $this->film_model->getAmountSeats($this->input->post('titel'));*/
        if($_POST == false)
        {
            $this->load->template('tickets/film_ticket');
        }
         else {
            $titel = $this->input->post('titel');
            $seat = $this->input->post('num-tickets');

            $data = array(
                'movie_titel' => $titel,
                'seats' => $seat
            );

            $this->film_model->add_ticket($data);
            $this->pay();
        }

    }

    public function pay()
    {
        $data = $this->film_model->get_ticket_data();
        $prijs = 7;
        $tot_prijs = $prijs * $data[0]->seats;

        $price= array(
            'prijs' => $tot_prijs
        );

        $this->film_model->alter_ticket($tot_prijs,$data[0]->titel,$data[0]->id);
        $this->load->template('tickets/betaal_film_tickets', $price);
    }
    public function print_ticket()
    {
        $data['query'] = $this->film_model->get_ticket_data();

        $this->load->template('tickets/print_film', $data);
    }
    public function get_pdf()
    {
        $this->load->library('mpdf');
        //get data from the model
        $data['query'] = $this->film_model->get_ticket_data();
        $mpdf = new mPDF('c', 'A4', '', '', 32, 25, 27, 25, 16, 13);
        $mpdf->SetDisplayMode('fullpage');

        //whether to indent the first level of a list
        $mpdf->list_indent_first_level = 0; // 1 or 0 -
        //load view for the pdf
        $html = $this->load->view('tickets/print_film_pdf', $data, TRUE);
        $mpdf->WriteHTML($html);
        $mpdf->Output('ticket.pdf', 'D');
    }
} 