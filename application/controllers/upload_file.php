<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Upload_file extends CI_Controller{


    function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
    }
    function index()
    {
        $this->load->manager_template('upload');
    }

    function upload_it() {
        //load the helper
        $this->load->helper('form');

        //Configure
        //set the path where the files uploaded will be copied. NOTE if using linux, set the folder to permission 777
        $config['upload_path'] = (APPPATH.'uploads/img');

        // set the filter image types
        $config['allowed_types'] = 'gif|jpg|png|mp4|avi|mkv';

        //load the upload library
        $this->load->library('upload', $config);

        $this->upload->initialize($config);

        $this->upload->set_allowed_types('*');

        $data['upload_data'] = '';

        //if not successful, set the error message
        if (!$this->upload->do_upload('userfile')) {
            $data = array('msg' => $this->upload->display_errors());
            echo $config['upload_path'];
        } else { //else, set the success message
            $data = array('msg' => "Upload success!");

            $data['upload_data'] = $this->upload->data();

        }
                //load the view/upload.php
        $this->load->manager_template('upload', $data);

        $file = array(
            'location' => $this->input->post('userfile'),

        );
        print_r($file);
        #$this->upload_model->add_file($file);
    }
    function do_upload()
    {
        $config['upload_path'] = '../DarwinMuseum/uploads/img';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '1000';
        $config['max_width']  = '';
        $config['max_height']  = '';
        $config['overwrite'] = TRUE;
        $config['remove_spaces'] = TRUE;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload())
        {
            $error = array('error' => $this->upload->display_errors());
            $this->upload_model->insert_images($this->upload->data());
            $this->load->manager_template('upload', $error);
        }
        else
        {
            $this->upload_model->insert_images($this->upload->data());
            $data = array('upload_data' => $this->upload->data());
            $this->load->manager_template('upload_succes', $data);
        }
    }

}
