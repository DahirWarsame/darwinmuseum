<?php
/**
 * Created by PhpStorm.
 * User: Eigenaar
 * Date: 17-11-2014
 * Time: 10:28
 */

class DisplayHook {
    public function captureOutput() {
        $this->CI =& get_instance();

        $output = $this->CI->output->get_output();
        if (ENVIRONMENT != 'testing') {
            echo $output;
        }
    }
}