<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Natural History Museum London</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/base.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.battatech.excelexport.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.battatech.excelexport.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
    <!--plugin to export table in .xls-->
    <script>
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport" , datatype: $datatype.Table
                });
            });
        });
    </script>
</head>
<body>
<div class="container">
    <div id="page-header">
        <a href="<?= site_url('/welcome/index'); ?>"><img src="<?php echo base_url(); ?>css/img/nhmLobeliaLogo.jpg" width="207" height="93" alt="Giant lobelia, Lobelia keniensis" id="page-logo"></a>
        <div id="nav-site-supplementary" class="menubar">
            <ul id="nav-site-supplementary-menu">
                <li class="research-curation"><a href="/research-curation/index.html" title="Science Directorate, Biodiversity research, Science enquiries, NHM Consulting, Research projects, Collections and library, Science facilities, Postgraduates, Collaborations, Departments, Staff directory">Research and curation</a></li>
                <li class="business-centre"><a href="/business-centre/index.html" title="Exclusive Events, Touring Exhibitions, Licensing, Publishing, Picture library, Filming, Photography, Planning and Design Consulting and Science Consulting services.">Business centre</a></li>
                <li class="about-us"><a href="/about-us/index.html" title="News, Contact and enquiries, Press office, Corporate information, Website help">About us</a></li>
            </ul>
        </div>
        <div id="nav-site" class="menubar">
            <!-- begin site navigation -->
            <ul id="nav-site-menu">
                <li class="home"><a href="<?= site_url('/verkoper/'); ?>">Home</a></li>
                <li class="home"><a href="<?= site_url('/tickets/addasVerkoper');?>">Bestel Tickets</a></li>
                <li class="home"><a href="<?= site_url('/verkoper/logout'); ?>">Logout</a></li>
            </ul>
        </div>
    </div>
    <!--- Contont begint hier -->
    <div class="col-md-*"></div>