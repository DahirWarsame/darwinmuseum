
<div id="page-footer">
    <div id="nav-footer" class="menubar">
        <ul id="nav-footer-menu">
            <li class="first-item">	<a href="/about-us/contact-enquiries/index.html">Contact and enquiries</a>
            </li>
            <li>	<a href="/about-us/website-help/accessibility/index.html">Accessibility</a>
            </li>
            <li>	<a href="/about-us/website-help/site-map/index.html">Site map</a>
            </li>
            <li>	<a href="/about-us/website-help/terms-of-use/index.html">Website terms of use</a>
            </li>


            <li id="page-copyright">© The Trustees of the Natural History Museum, London</li>
            <li id="page-cookies"><a href="/about-us/website-help/terms-of-use/cookies/index.html">Information about cookies</a></li>
            <li><a onclick="recordEvent('/index.html', 'Template change', 'mobile')" href="/index.html?site-version=mobile">Mobile</a></li>
            <li><p class="footer">Render Time {elapsed_time}</p></li>
        </ul>
    </div>
</div>
<!--- Div voor containter classe -->
</div>

</body>
</html>