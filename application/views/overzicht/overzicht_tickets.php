<div class="col-md-8">
    <p>Welkom op de overzichten pagina. Op deze pagina kan de manager verschillende overzichten zien van de museum.</p>
    <p>Klik op een van de links hiernaast om een overzicht te zien van de tickets.</p>
</div>
<div class="col-md-4">
    <ul class="nav nav-pills nav-stacked">
        <li role="presentation" class="active"><a href="<?= site_url('/overzicht/tickets_today'); ?>">Overzicht van vandaag</a></li>
        <li role="presentation"><a href="<?= site_url('/overzicht/tickets_lastweek');?>">Overzicht van vorige week</a></li>
        <li role="presentation"><a href="<?= site_url('/overzicht/tickets_last_3months');?>">Overzicht van 3 maanden geleden</a></li>
    </ul>
</div>
<div class="col-md-4">

</div>