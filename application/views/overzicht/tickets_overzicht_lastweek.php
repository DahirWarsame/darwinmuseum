<div class="col-md-8">
    <table id="tblExport" class="table table-bordered">
        <thead>
        <tr>
            <td>Datum</td>
            <td>Order Num</td>
            <td>Naam</td>
            <td>Tussenvoegsel</td>
            <td>achternaam</td>
            <td>geboortedatum</td>
            <td>woonplaats</td>
            <td>postcode</td>
            <td>prijs</td>
        </tr>
        </thead>
        <tbody>
        <?php  $num = 1;
        foreach($tickets as $tickets) { ?>
            <tr>
                <td><?php echo $tickets->gekocht;?></td>
                <td><?php echo $num;?></td>
                <?php $num++; ?>
                <td><?php echo $tickets->naam;?></td>
                <td><?php echo $tickets->tussenvoegsel;?></td>
                <td><?php echo $tickets->achternaam;?></td>
                <td><?php echo $tickets->geboortedatum;?></td>
                <td><?php echo $tickets->woonplaats;?></td>
                <td><?php echo $tickets->postcode;?></td>
                <td><?php echo $tickets->prijs;?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>

    <div>
        <button id="btnExport">Export to excel</button>
    </div>
</div>      
