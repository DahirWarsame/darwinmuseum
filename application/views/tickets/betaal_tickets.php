
<div class="">
        <div class="form-group"><strong>iDEAL</strong> - Online betalen via je Nederlandse bank
            <div class="preferred_payment_method">Meest gebruikte betaalmethode in Nederland</div>
        </div>
        <?php echo form_open('tickets/print_ticket'); ?>
        <div class="form-group">
            <label for="prijs">Prijs</label>
            <input type="text" class="form-control" value=" <?php echo $prijs;?>" id="prijs" name="prijs" disabled>
        </div>
        <label for="ideal_bank" class="bottom_xxs">
            <span class="labeltext">Bank*</span>
            <select id="ideal_bank" name="idealform" tabindex="3" class="form-control">
                <option value="" selected="selected">Kies je bank...</option>
                <option value="ABN">ABN Amro Bank</option>
                <option value="ASN">ASN Bank</option>
                <option value="ING">ING</option>
                <option value="KNAB">Knab</option>
                <option value="RABO">Rabobank</option>
                <option value="RBRB">RegioBank</option>
                <option value="SNSB">SNS Bank</option>
            </select>
        </label>
    <fieldset class="sendform box_radius4_bottom" data-element-type="inline_payment_button">
        <div class="fleft boxedright_m js_tooltip_holder" data-tooltip="Kies eerst je bank.">
            <input id="submitcheck_payment" name="_eventId_submitPayment" value="Bestelling plaatsen" class="btn btn-default" tabindex="9" type="submit"><span></span>
        </div>
        <div class="grid_477 fleft small_details" style="padding-top: 3px;">
            <div class="infotext">
                <p class="bottom_0"><strong>Klik op Bestelling plaatsen om de bestelling definitief te maken.</strong>
                </p>
            </div>
        </div>
    </fieldset>
    <?= form_close();?>
</div>