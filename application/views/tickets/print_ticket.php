<h1>History Museum Te London</h1>
<h2>Ticket</h2>
<?php $code = random_string('numeric', 7); ?>
<table class="items" width="100%" cellpadding="8" border="1">
    <!-- ITEMS HERE -->
    <?php foreach($query as $query) { ?>
        <tr>
            <td><?php echo "Ticket Nummer"?></td>
            <td><?php echo $query->id;?></td>
        </tr>
        <tr>
            <td><?php echo "Naam";?></td>
            <td><?php echo $query->naam . " " . $query->tussenvoegsel . " " . $query->achternaam;?></td>
        </tr>
        <tr>
            <td><?php echo "Woonplaats"?></td>
            <td><?php echo $query->woonplaats;?></td>
        </tr>
        <tr>
            <td><?php echo "PostCode"?></td>
            <td><?php echo $query->postcode;?></td>
        </tr>
        <tr>
            <td><?php echo "Geboortedatum";?></td>
            <td><?php echo $query->geboortedatum;?></td>
        </tr>
        <tr>
            <td><?php echo "Leeftijd";?></td>
            <td><?php
                $from = new DateTime($query->geboortedatum);
                $to   = new DateTime('today');
                $age = $from->diff($to)->y;
                echo $age
                ?></td>
        </tr>

    <?php } ?>
</table>
<br>
<a href="<?= site_url('/tickets/get_pdf'); ?>"><button type="button" class="btn btn-primary">Print</button></a>