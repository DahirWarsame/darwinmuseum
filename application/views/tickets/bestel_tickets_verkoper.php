
<?php echo form_open('tickets/addasVerkoper'); ?>
    <div class="form-group">
        <label for="naam">Naam</label>
        <input type="text" class="form-control" value="<?php echo set_value('naam');?>" id="naam" name="naam" placeholder="Enter Name" required>
    </div>
    <div class="form-group">
        <label for="tussenvoegsel">Tussenvoegsel</label>
        <input type="text" class="form-control" value="<?php echo set_value('tussenvoegsel');?>" id="tussenvoegsel" name="tussenvoegsel" placeholder="Tussenvoegsel" required>
    </div>
    <div class="form-group">
        <label for="achternaam">Achternaam</label>
        <input type="text" class="form-control" value="<?php echo set_value('achternaam');?>" id="achternaam" name="achternaam" placeholder="Achternaam" required>
    </div>
    <div class="form-group">
        <label for="geboortedatum">Geboortedatum (jjjj-mm-dd)</label>
        <input type="date" class="form-control" value="<?php echo set_value('geboortedatum');?>" id="geboortedatum" name="geboortedatum" placeholder="jjjj-mm-dd" required>
    </div>
    <div class="form-group">
        <label for="woonplaats">Woonplaats</label>
        <input type="text" class="form-control" value="London" id="woonplaats" name="woonplaats" placeholder="LONDON" disabled required>
    </div>
    <div class="form-group">
        <label for="postcode">Postcode</label>
        <input type="text" class="form-control" value="1234AB" id="postcode" name="postcode" placeholder="1234ab" disabled required>
    </div>
    <button type="submit" class="btn btn-default">Submit</button>
<?= form_close();?>