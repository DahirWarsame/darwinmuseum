
<?php echo form_open('tickets/add'); ?>
    <div class="form-group">
        <label for="naam">Naam</label>
        <input type="text" class="form-control" value="<?php echo set_value('naam');?>" id="naam" name="naam" placeholder="Enter Name" >
    </div>
    <div class="form-group">
        <label for="tussenvoegsel">Tussenvoegsel</label>
        <input type="text" class="form-control" value="<?php echo set_value('tussenvoegsel');?>" id="tussenvoegsel" name="tussenvoegsel" placeholder="Tussenvoegsel">
    </div>
    <div class="form-group">
        <label for="achternaam">Achternaam</label>
        <input type="text" class="form-control" value="<?php echo set_value('achternaam');?>" id="achternaam" name="achternaam" placeholder="Achternaam">
    </div>
    <div class="form-group">
        <label for="geboortedatum">Geboortedatum (jjjj-mm-dd)</label>
        <input type="date" class="form-control" value="<?php echo set_value('geboortedatum');?>" id="geboortedatum" name="geboortedatum" placeholder="jjjj-mm-dd">
    </div>
    <div class="form-group">
        <label for="woonplaats">Woonplaats</label>
        <input type="text" class="form-control" value="<?php echo set_value('woonplaats');?>" id="woonplaats" name="woonplaats" placeholder="Woonplaats">
    </div>
    <div class="form-group">
        <label for="postcode">Postcode</label>
        <input type="text" class="form-control" value="<?php echo set_value('postcode');?>" id="postcode" name="postcode" placeholder="1234ab">
    </div>
    <button type="submit" class="btn btn-default">Submit</button>
<?= form_close();?>