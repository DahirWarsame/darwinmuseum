<div class="col-md-6">
<?php echo form_open('film_ticket/addFilmTicket'); ?>
    <div class="form-group">
        <label for="titel">Film Titel</label>
        <select id="titel" name="titel">
        <?php foreach($movies as $movies) { ?>
            <option value="<?php echo $movies->titel; ?>"><?php echo $movies->titel ." ". $movies->speeling; ?></option>
    <?php } ?>
            
        </select>
    </div>
    <div class="form-group">
        <label for="tussenvoegsel">Aantal Tickets (1 t/m 5)</label>
        <input type="number" class="form-control" min="1" max="5" step="1" value="1" name="num-tickets">
    </div>
    <button type="submit" class="btn btn-default">Submit</button>
<?= form_close();?>
    </div>