<p>Dit zijn alle Personeelsleden</p>
<table id="tblExport" class="table table-bordered">
    <thead>
    <tr>
        <td>Naam</td>
        <td>Achternaam</td>
        <td>Functie</td>
    </tr>
    </thead>
    <tbody>
    <?php foreach($users as $users) { ?>
        <tr>
            <td><?php echo $users->naam;?></td>
            <td><?php echo $users->achternaam;?></td>
            <td><?php echo $users->functie;?></td>
        </tr>
    <?php } ?>
    </tbody>
</table>