<div class="row">
    <div class="col-lg-6">
        <h1>Contact</h1>
        <p><b>Natural History Museum<br></b>Cromwell Road&nbsp;&nbsp;London&nbsp;&nbsp;SW7 5BD&nbsp;&nbsp;UK.
            <br>Tel: +44 (0)20 7942 5000
            <br>
            <a href="https://maps.google.co.uk/maps?ie=UTF-8&amp;q=natural+history+museum&amp;fb=1&amp;gl=uk&amp;hq=natural+history+museum&amp;hnear=0x47d8a00baf21de75:0x52963a5addd52a99,London&amp;cid=0,0,8914844151054820548&amp;ei=YhevUZyDLeyp0AXjwoCICA&amp;ved=0CKEBEPwSMAA" target="_blank"><b>Find us on a map</b></a>
        </p>
        <p>Open <b>Monday to Sunday</b> from 10.00-17.50, except 24-26 December. Last admission 17.30.</p>
        <p><b>The Museum at Tring</b> is open Monday to Saturday 10.00-17.00 and Sunday 14.00-17.00.</p>

    </div>
    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2483.8993214337793!2d-0.176367!3d51.496714999999995!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x7bb7e385c39764c4!2sNatural+History+Museum!5e0!3m2!1sen!2suk!4v1416215519574" width="600" height="450" frameborder="0" style="border:0"></iframe>
</div>
