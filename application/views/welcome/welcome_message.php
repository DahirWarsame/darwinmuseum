<div id="home" class="col-md-7">
	<h1>Welcome to The Natural History Museum!</h1>
    <div class="panel about-us" id="panel-one">
        <h2>New <i>Stegosaurus</i> fossil specimen to go on display</h2>
        <div class="panel-intro">
            <div class="image left" style=" width: 250px;">
                <a href="/about-us/news/2014/nov/rare-stegosaurus-skeleton-to-be-unveiled-at-the-museum133542.html" onclick="nhmTracker('/Promos-GA/steg/Homepanelintro/007/index.html');"><img alt="Uncover the stories behind our newest ancient resident." src="http://www.nhm.ac.uk/resources/homepage/images/steg-250.jpg">
                </a>
            </div>
            <p>Uncover the stories behind our newest exhibit - the <b>most complete <i>Stegosaurus</i> fossil ever found</b>.</p>
            <ul style=" margin-left: 270px;">
                <li><a href="/nature-online/life/dinosaurs-other-extinct-creatures/most-complete-stegosaurus-specimen/index.html"><b>All about the specimen</b></a>
                </li>
                <li><a href="/nature-online/life/dinosaurs-other-extinct-creatures/most-complete-stegosaurus-specimen/interview-with-dinosaur-expert/index.html"><b>Q&amp;A: Prof Paul Barrett</b></a>
                </li>
                <li><a href="/nature-online/life/dinosaurs-other-extinct-creatures/most-complete-stegosaurus-specimen/revealing-stegosaurus-secrets/index.html"><b>Secrets of the <em>Stegosaurus</em></b></a>
                </li>
                <li><a href="/visit-us/galleries/red-zone/stegosaurus-earth-hall/index.html"><b>Visit the <i>Stegosaurus</i> soon</b></a>
                </li>
            </ul>
        </div>
        <!-- end div.panel-intro -->
        <div class="panel-content">
            <div class="panel-content">
                <div style=" padding-left: 0px; padding-right: 0px; padding-bottom: 0px; margin-bottom: 10px; float: right; padding-top: 0px; width: 204px; border-left: #fff 1px solid;">
                    <p><b>Quick links</b>:</p>
                    <ul style=" margin-left: 10px;">
                        <li><a href="/visit-us/getting-here/index.html">Getting here</a>
                        </li>
                        <li><a href="/tickets/events-listing.html">Book tickets</a>
                        </li>
                        <li><a href="/visit-us/galleries/gallery-announcements/index.html">Gallery announcements</a>
                        </li>
                        <li><a href="/take-part/enews-sign-up/index.html">Sign up for news</a>
                        </li>
                        <li><a href="/nature-online/life/dinosaurs-other-extinct-creatures/dino-directory/index.html">Dino Directory</a>
                        </li>
                        <li><a href="/about-us/jobs-volunteering-internships/index.html" title="">Jobs and volunteering</a>
                        </li>
                    </ul>
                    <ul class="social" style=" margin-left: 10px;">
                        <li class="facebook"><a href="http://www.facebook.com/pages/London-United-Kingdom/Natural-History-Museum-London/198081311536 ">Join us on Facebook</a>
                        </li>
                        <li class="twitter"><a href="http://twitter.com/NHM_London">Follow us on Twitter</a>
                        </li>
                        <li class="youtube"><a href="http://www.nhm.ac.uk/youtube">Watch us on YouTube</a>
                        </li>
                    </ul>
                    <p>For any emergency updates go to <a href="http://www.facebook.com/pages/London-United-Kingdom/Natural-History-Museum-London/198081311536">Facebook</a> or <a href="http://twitter.com/NHM_London">Twitter</a>.</p>
                </div>
                <div style=" border-bottom-width: 0px; float: left; width: 280px;">
                    <h3>Entry is free</h3>
                    <p>There is a charge for some exhibitions.</p>
                    <p><b>Natural History Museum<br></b>Cromwell Road&nbsp;&nbsp;London&nbsp;&nbsp;SW7 5BD&nbsp;&nbsp;UK.
                        <br>Tel: +44 (0)20 7942 5000
                        <br><a href="https://maps.google.co.uk/maps?ie=UTF-8&amp;q=natural+history+museum&amp;fb=1&amp;gl=uk&amp;hq=natural+history+museum&amp;hnear=0x47d8a00baf21de75:0x52963a5addd52a99,London&amp;cid=0,0,8914844151054820548&amp;ei=YhevUZyDLeyp0AXjwoCICA&amp;ved=0CKEBEPwSMAA" target="_blank"><b>Find us on a map</b></a>
                    </p>
                    <p>Open <b>Monday to Sunday</b> from 10.00-17.50, except 24-26 December. Last admission 17.30.</p>
                    <p><b>The Museum at Tring</b> is open Monday to Saturday 10.00-17.00 and Sunday 14.00-17.00.</p><p> <a href="http://www.nhm.ac.uk/tring/index.html" title="Tring website link">Tring website</a>.</p>
                    <p><b>Cookies</b>
                        <br><a href="http://www.nhm.ac.uk/about-us/website-help/terms-of-use/cookies/index.html">Read our latest information on cookies</a>.</p>
                </div>
            </div>
            <br>
            <div class="image right" style="width:62px">
                <img src="<?= base_url(); ?>img/natureplus-62-94462-1.jpg" alt="NaturePlus" width="62" height="62">
            </div>
            <ul>
                <li>Explore <a href="http://www.nhm.ac.uk/natureplus"><b>NaturePlus</b></a> blogs and forums. <a href="https://www.nhm.ac.uk/sso/login?service=http%3A%2F%2Fwww.nhm.ac.uk%2Fnatureplus%2Flogin.jspa%3FfromCas%3Dtrue">Sign in</a> or <a href="https://www.nhm.ac.uk/my-nhm/register/natureplus.html">register</a> to join in.</li>
                <li><a href="http://www.nhm.ac.uk/natureplus/community/identification">Get help identifying plants, animals and rocks</a> and more.</li>
                <li>What's happening behind the scenes? <a href="http://www.nhm.ac.uk/natureplus/blogs/whats-new">What's new blog</a>.</li>
            </ul>
        </div>
        <!-- end div.panel-content -->
    </div>
</div>

<div class="col-md-4">
    <div class="list-group">
        <a href="#" class="list-group-item active">
            <h4 class="list-group-item-heading">Speeltijden</h4>
            <p class="list-group-item-text">Hieronder zie je alle speel tijden</p>
        </a>
            <?php foreach($movies as $movies) { ?>
                <a href="#" class="list-group-item">
                    <h4 class="list-group-item-heading"><?php echo $movies->titel; ?></h4>
                    <p class="list-group-item-text">Deze film is te zien op <?php echo $movies->speeling; ?>
                    <b style="color: green;">STOELEN ZIJN BESCHIKBAAR</b>
                    </p>
                </a>
            <?php
            }
            ?>
    </div>
</div>


