<div class="row">

    <div class="col-lg-12">
        <h1 class="page-header">Thumbnail Gallery</h1>
    </div>
    <?php foreach($files as $files) { ?>
    <div class="col-lg-3 col-md-4 col-xs-6 thumb">
        <a class="thumbnail" href="<?= site_url('/upload/delete/'.$files->id); ?>">
        <?php 
        if($files->type == 'image/jpeg')
        {
        ?>
            <img class="img-responsive" src="<?="http://localhost/darwinmuseum/uploads/img/".$files->location;?>" alt="">
        <?php } else {?> 
            <video width="320" height="240" autoplay>
              <source src="<?="http://localhost/darwinmuseum/uploads/img/".$files->location;?>" type="<?php echo $files->type;  ?>">
            </video>
            <?php } ?>
        </a>
    </div>
    <?php } ?>
</div>