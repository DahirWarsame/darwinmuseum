<form action="<?=site_url('user/create_user')?>" method="post" class="form-signin" role="form" accept-charset="utf-8">
<h2 class="form-signin-heading">Please sign in</h2>
<label for="inputEmail" class="sr-only">Email address</label>
<input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
<label for="inputPassword" class="sr-only">Password</label>
<input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
<input class="btn btn-lg btn-primary btn-block" type="submit" value="Sign in">
<?= form_close();?>