<?php
/**
 * Created by PhpStorm.
 * User: Eigenaar
 * Date: 19-11-2014
 * Time: 20:44
 */

class TicketTest extends PHPUnit_Framework_TestCase {
    private $CI;

    public function setUp() {
        $this->CI = &get_instance();
    }

    public function testGetAllPosts() {
        $this->CI->load->model('ticket');
        $ticket = $this->CI->post->getAll();
        $this->assertEquals(5, count($ticket));
    }
}